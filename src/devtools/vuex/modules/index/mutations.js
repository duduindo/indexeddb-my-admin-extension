export default {
  SET_INDEX_CONTENT: (state, { data }) => {
    state.content = data
  }
}
