export default {
  SET_HOST: (state, { data }) => {
    state.host = data
  },
  SET_ID: (state, value) => {
    state.id = value
  }
}
