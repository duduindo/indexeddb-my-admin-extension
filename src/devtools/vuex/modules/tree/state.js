export default {
  databases: [
    {
      // host: 'localhost:8443', // Este é o certo
      host: 'pcbafhomjbkcokpgfhjhhpbcmgkajnhn',
      name: 'gih-reservations',
      version: 10
    },
    {
      host: 'localhost:8443',
      name: 'gih-reservations',
      version: 10
    },
    {
      host: 'localhost:8443',
      name: 'database1',
      version: 2
    },
    {
      host: 'www.oantagonista.com',
      name: 'ONE_SIGNAL_SDK_DB',
      version: 1
    },
    {
      host: 'www.google.com',
      name: 'database-google',
      version: 845
    },
    {
      host: 'jsperf.com', // https://jsperf.com/indexeddb-vs-localstorage/69
      name: 'IDBWrapper-jsperfteststore',
      version: 1
    },
    {
      host: 'jsperf.com', // https://jsperf.com/indexeddb-vs-localstorage/69
      name: 'IDBWrapper-perf',
      version: 1
    },
    {
      host: 'www.netflix.com',
      name: 'netflix.player',
      version: 1
    }
  ],

  tree: []
}
