export default {
  getObjectStoreContent: state => state.content,
  getObjectStoreInsertedStatus: state => state.statusInserted,
  getObjectStoreDeletedStatus: state => state.statusDeleted
}
