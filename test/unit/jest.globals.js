import IndexedDBFake from 'fake-indexeddb'
import IDBKeyRange from 'fake-indexeddb/lib/FDBKeyRange'


global.indexedDB = IndexedDBFake
global.IDBKeyRange = IDBKeyRange
